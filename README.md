# Sample API Tests using Supertest

## Steps to Run: 
1. npm i
2. npm run test

## Notes
- Test uses jest and supertest
- There are 4 types of tests (as defined in test/get.test.js):
  1. Get the all the data related to insurance
  2. Get etymologies only for the word insurance
  3. 400 Status
  4. 404 Status
- data folder a snapshot of what the API currently returns for etymology only. Data is used
  - partially in Test #1
  - to do full check in Test #2