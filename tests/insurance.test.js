require('dotenv').config();
const supertest = require('supertest');
const expectedResult = require(`../data/expected_definitions.json`);

// Define the variables to be used in test
const request = supertest(process.env.BASE_URL);
const header = {
  app_id: process.env.APP_ID,
  app_key: process.env.APP_KEY
}

describe(`Test Oxford Dictionaries - Get Definitions`, () => {
  it('Get entire definition', () => request
    .get(`api/v2/entries/en-gb/Insurance`)
    .set(header)
    .expect(200)
    .then(res => {

      // Check that we're getting a result
      expect(res.body).toHaveProperty('results');
      expect(res.body.results).toHaveProperty('0');
      actualResult = res.body.results[0];

      // Check the id
      expect(actualResult).toHaveProperty('id');
      expect(actualResult.id).toEqual(expectedResult.id);

      // Check etymologies
      expect(actualResult).toHaveProperty('lexicalEntries');
      expect(actualResult.lexicalEntries).toHaveProperty('0');
      expect(actualResult.lexicalEntries[0]).toHaveProperty('entries');
      expect(actualResult.lexicalEntries[0].entries).toHaveProperty('0');
      expect(actualResult.lexicalEntries[0].entries[0]).toHaveProperty('etymologies');
      expect(actualResult.lexicalEntries[0].entries[0].etymologies).toEqual(expectedResult.lexicalEntries[0].entries[0].etymologies);

      // Check language
      expect(actualResult.lexicalEntries[0]).toHaveProperty('language');
      expect(actualResult.lexicalEntries[0].language).toEqual(expectedResult.lexicalEntries[0].language);
    })
  );

  it('Get only etymologies', () => request
    .get(`api/v2/entries/en-gb/Insurance`)
    .set(header)
    .query({fields: 'etymologies'})
    .expect(200)
    .then(res => {
      // Check that the result matches exactly to what we have on record
      expect(res.body).toHaveProperty('results');
      expect(res.body.results).toHaveProperty('0');
      expect(res.body.results[0]).toEqual(expectedResult);
    })
  );
});

describe(`Test Oxford Dictionaries - Negative Tests`, () => {
  it('404 response', () => request
    .get(`api/v2/entries/en-gb/Insuranc`)
    .set(header)
    .expect(404)
  );
  
  it('400 response', () => request
    .get(`api/v2/entries/en-gb/Insurance`)
    .set(header)
    .query({field: 'etymologies'})
    .expect(400)
  );
});